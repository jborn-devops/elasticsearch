FROM docker.elastic.co/elasticsearch/elasticsearch:7.7.0

COPY elasticsearch.yml /usr/share/elasticsearch/config/elasticsearch.yml